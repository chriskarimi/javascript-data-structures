# README #

This project is focused solely on developing data-structures and algorithms using JavaScript. Right now we're still in v1 of releases, so we're in the process of creating thorough documentation to go along with each data-structure as well as building them along side. 

To get this project up and running from a pull, you can use MAMP / WAMP to create a local server and run it in the browser. View results in the console. The goal for v2 of this project is to have a visual representation for the different data-structures as well as the algorithms that go along with them. 

### Contribution guidelines ###

Right now as this project is still new, we don't expect to have many contributions, but if you find yourself looking for a project like this to learn data-structures and algorithms in JavaScript, you've come to the right place! 

If there's a change to a current data-structure you think can be improved or would like to contribute a data-structure not already in the list, please do a pull request and we will merge with the next release.

### Who do I talk to? ###

* Chris Karimi - chriskarimi@gmail.com
* Tim Anderson - mindfullsilence@gmail.com