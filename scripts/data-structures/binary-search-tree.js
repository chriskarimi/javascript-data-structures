"use strict";
var BinarySearchTree = function () {
  this.root = null
  this.length = 0
}

BinarySearchTree.prototype.closest = function (value) {
  var self = this
  var closest
  var findClosest = (val, node) => {
    if (node === null) { // only when root is null
      return null
    }
    if (val === node.data) {
      return node.parent === null ? node : node.parent
    }
    if (val > node.data && node.right === null) {
      return node
    }
    if (val < node.data && node.left === null) {
      return node
    }
    if (node.data < val && val > node.right.data) {
      return node
    }
    if (node.data > val && val < node.left.data) {
      return node
    }
    if (val < node.data) {
      return findClosest(val, node.left)
    }
    if (val > node.data) {
      return findClosest(val, node.right)
    }
  }
  closest = findClosest(value, self.root)
  return closest
}

BinarySearchTree.prototype.search = function (value) {
  var finder = (val, node) => {
    if (node === null) {
      return null
    }
    else if (node.data === val) {
      return node
    }
    else if (val < node.data && node.left !== null) {
      return finder(val, node.left)
    }
    else if (val > node.data && node.right !== null) {
      return finder(val, node.right)
    }
    else if (val < node.data && node.left === null) {
      return null
    }
    else if (val > node.data && node.right === null) {
      return null
    }
  }

  return finder(value, this.root)
}

BinarySearchTree.prototype.insert = function (value) {
  var self = this
  var inserted
  var parent
  var newNode = (val, parent) => {
    self.length++
    return {
      data: val,
      left: null,
      right: null,
      parent: parent || null
    }
  }
  var deepest = (val, node) => {
    if (val < node.data) {
      if (node.left === null) {
        return node
      }
      return deepest(val, node.left)
    } else {
      if (node.right === null) {
        return node
      }
      return deepest(val, node.right)
    }
  }
  if (this.contains(value)) {
    return false
  }
  if (this.root === null) {
    this.root = newNode(value, null)
    return this.root
  }
  if (this.contains(value)) {
    return this.search(value)
  }
  parent = deepest(value, this.root)
  inserted = newNode(value, parent)
  if (value < parent.data) {
    parent.left = inserted
  }
  if (value > parent.data) {
    parent.right = inserted
  }
  return inserted
}

BinarySearchTree.prototype.contains = function (value) {
  var self = this
  if (self.root === null) {
    return false
  }
  return !(this.search(value) === null)
}

BinarySearchTree.prototype.shiftChildNodes = function (node) {
  if (node.data !== null) {
    return false
  }
  if (node.right === null && node.left === null) {
    this.length--
    if (node.parent === null) {
      this.root = null
    }
    else if (node.parent.left === node) {
      node.parent.left = null
    }
    else if (node.parent.right === node) {
      node.parent.right = null
    }
    return true
  }
  if (node.right !== null) {
    node.data = node.right.data
    node.right.data = null
    return this.shiftChildNodes(node.right)
  } else {
    node.data = node.left.data
    node.left.data = null
    return this.shiftChildNodes(node.left)
  }
}

BinarySearchTree.prototype.delete = function (value) {
  var node = this.search(value)
  if (node === null) {
    return false
  }
  node.data = null
  return this.shiftChildNodes(node)
}

BinarySearchTree.prototype.getFlat = function () {
  var arr = []
  var grepVal = (node) => {
    if (node.data === null) {
      console.log(node)
    }
    if (node.left !== null) {
      grepVal(node.left)
    }
    if (node.right !== null) {
      grepVal(node.right)
    }
    arr.push(node.data)
  }
  grepVal(this.root)
  return arr
}

BinarySearchTree.prototype.sort = function () {
  var arr = this.getFlat()
  arr.sort((a, b) => a - b)
  this.root = null
  for (let i = 0; i < arr.length; i++) {
    this.insert(arr[i])
  }
}
