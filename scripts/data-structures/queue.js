/*
 Author: Chris Karimi - chriskarimi@gmail.com
 Date: 9/19/2016
 */

/*
 QUEUE - A linear data structure. A collection of components arranged in a straight line.

 FIFO = First In First Out

 The same as waiting in a line.

 Usage: Great for tasks builders where you need to complete tasks in the order they come in. The first task that comes in
 should be the first task to leave.
 */

function Queue() {
  this.length = 0;
  this.items = [];
}

/*
 Average: O(n)
 Worst: O(n)
 */
Queue.prototype.access = function(value) {

  // Nothing to access, return -1
  if(this.length === 0) {
    return -1;
  }

  var i = 0;

  for( ; i < this.length; i++ ) {

    if( this.items[i] === value ) {
      return i;
    }
  }

  // No value found, return -1.
  return -1;
};

/*
 Average: O(n)
 Worst: O(n)
 */
Queue.prototype.search = function(value) {

  // Nothing to access, return -1
  if(this.length === 0) {
    return false;
  }

  var i = 0;

  for( ; i < this.length; i++ ) {

    if( this.items[i] === value ) {
      return true;
    }
  }

  // No value found, return -1.
  return false;
};

/*
 Average: O(1)
 Worst: O(1)
 */
Queue.prototype.insert = function(item) {
  this.length++;
  this.items.push(item);
};

/*
 Average: O(1)
 Worst: O(1)
 */
Queue.prototype.delete = function() {

  // No items to delete.
  if(this.length == 0) {
    return -1;
  }

  return this.items.shift();
};

/*
Misc. Function to print Queue.
 */
Queue.prototype.print = function() {
  console.log(this.items);
};