/*
 Author: Chris Karimi - @chriskarimi - chriskarimi@gmail.com
 Date: 9/22/2016
 */

function LinkedList() {
  this.length = 0;
  this.head = null;
}

LinkedList.prototype.contains = function(val) {

  // Set current to the head so we have a temporary var to skip through and not lose our head placement.
  var current = this.head;

  while( current !== null ) {

    if( current.data === val ) {
      return true;
    }

    current = current.next;
  }

  return false;
};

LinkedList.prototype.prepend = function(val) {

  if( this.contains(val) ) {
    return;
  }

  this.head = {
    data: val,
    next: this.head
  };

  this.length++;
};

LinkedList.prototype.append = function(val) {

  if( this.contains(val) ) {
    return;
  }

  var newNode = {
    data: val,
    next: null
  };

  var current = this.head;

  // No elements in the list, set head to new node as first node in the list.
  if( current === null ) {
    this.head = newNode;
    this.length++;
    return;
  }

  // Continue through the list until you reach a node where the next link is null. That's the last node in the list.
  while( current.next !== null ) {
    current = current.next;
  }

  current.next = newNode;
  this.length++;
};

LinkedList.prototype.delete = function(val) {

  var current = this.head,
    previous = null;

  // If no elements in the list or the value is not in the list, return.
  if( current == null || !this.contains(val) ) {
    return;
  }

  while( current !== null ) {
    // Found value in the first item in the list.
    if( current.data === val && previous === null ) {
      this.head = current.next;
      break;
    } else if ( current.data == val ) {
      // Set the previous nodes next node of the deleted item linking past it and removing it's reference in the list.
      previous.next = current.next;
      break;
    } else {
      // Keep track of previous node so if we find the value, we can use it to link past the deleted node.
      previous = current;
      current = current.next;
    }
  }

  this.length--;
};