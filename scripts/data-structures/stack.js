
function Stack() {
  this.length = 0;
  this.items = [];
}

/*
 Average: O(n)
 Worst: O(n)
 */
Stack.prototype.access = function(value) {

  var i = 0;

  // Nothing to access, return -1
  if(this.length === 0) {
    return -1;
  }

  for( ; i < this.length; i++ ) {
    if( this.items[i] === value ) {
      return i;
    }
  }

  // No value found, return -1.
  return -1;
};

/*
 Average: O(n)
 Worst: O(n)
 */
Stack.prototype.search = function(value) {

  var i = 0;

  // Nothing to access, return false
  if(this.length === 0) {
    return false;
  }

  for( ; i < this.length; i++ ) {
    if( this.items[i] === value ) {
      return true;
    }
  }

    // No value found, return false.
    return false;
  };

/*
 Average: O(1) - Constant speed, very fast.
 Worst: O(1) - Constant speed, very fast.
 */
Stack.prototype.insert = function(item) {

  this.length++;
  this.items.push(item);
};

/*
 Average: O(1) - Constant speed, very fast.
 Worst: O(1) - Constant speed, very fast.
 */
Stack.prototype.delete = function() {

  if(this.length == 0) {
    return -1;
  }

  this.length--;
  return this.items.pop();
};


/*
Misc. Function to print stack.
 */
Stack.prototype.print = function() {
  console.log(this.items);
};