
:::::::::::::::::::::::::::::::::::::::::::::::::::::

To use test files in this folder, they've been including in the index.html, however you will just need to uncomment them as you. To see outputs, you will need to add your own console.logs to verify data. 

We're currently in progress of working on developing more thorough unit tests and full test-cases per data structure, but most likely that will come around v2.0. 

If you have any questions, comments or concerns, please email me or post on the repo and we will try to assist. 

Thanks!
