/*
 Timing function used to test running time with various data sizes.
 */


var test_size = 50000,
  i = 0;

var start = new Date().getTime();

for ( ; i < test_size; i++ ) {

  // Run functions in here.

}

var end = new Date().getTime();
var time = end - start;

console.log('Execution time: ' + time);
